#!/usr/bin/env node

const express = require('express');
const bodyParser = require('body-parser')
const app = express();

app.use(bodyParser.json());

app.post('/echo', (req, res) => {
  res.json(req.body);
});

app.all('*', (req, res) => {
  res.json({
    path: req.path,
    headers: req.headers,
    method: req.method,
    body: req.body,
    cookies: req.cookies,
    fresh: req.fresh,
    hostname: req.hostname,
    ip: req.ip,
    ips: req.ips,
    protocol: req.protocol,
    query: req.query,
    subdomains: req.subdomains,
    xhr: req.xhr,
  });
});

const port = process.env.PORT || 3000;
console.log(`Listening on port ${port}...`);
app.listen(port);
