# Development HTTP Echo

```bash
docker run -it --rm -e "PORT=1234" -p "1234:1234" gregbacchus/echo
```

POST requests to `http://localhost:1234/echo` will return the JSON request body as the response body.

All other requrests will return JSON information about the request, e.g.

```json
{
    "path": "/",
    "headers": {
        "content-type": "application/json",
        "cache-control": "no-cache",
        "postman-token": "cc0e23f9-5e46-4634-bd36-daafa21d9fd4",
        "user-agent": "PostmanRuntime/7.1.1",
        "accept": "*/*",
        "host": "localhost:3000",
        "accept-encoding": "gzip, deflate",
        "content-length": "14",
        "connection": "keep-alive"
    },
    "method": "POST",
    "body": {
        "foo": "bar"
    },
    "fresh": false,
    "hostname": "localhost",
    "ip": "::ffff:172.17.0.1",
    "ips": [],
    "protocol": "http",
    "query": {},
    "subdomains": [],
    "xhr": false
}
```